(function($)
{
	$(document).ready(function()
	{
		$("input[type='checkbox'],input[type='radio']").customInput();

		$("select").customSelect();

		$("#Menu nav a").click(function(event)
		{
			event.preventDefault();
			var id = $(this).attr("href");
			$('html,body').animate({
	        	scrollTop: $(id).offset().top - 106
	        }, 600);
	        if ($("#Menu-Toggle").hasClass("Clicked")){
	        	$("#Menu nav").slideUp(200);
				$("#Menu-Toggle").removeClass("Clicked");
	        }
		});

		$("#Zdjecia a").colorbox(
		{
			href: this.src,
            maxHeight: "90%",
            maxWidth: "90%",
            rel: "gallery"
		});

		$("#Impreza").colorbox(
		{
			href: this.src,
            maxHeight: "90%",
            maxWidth: "90%",
            rel: "impreza"
		});

		$("#Menu-Toggle").click(function(){
			if ($(this).hasClass("Clicked")){
				$("#Menu nav").slideUp(200);
				$(this).removeClass("Clicked");
			}
			else{
				$("#Menu nav").slideDown(200);
				$(this).addClass("Clicked");
			}
		});

    var siteKey = '6Le5IKgUAAAAAF5PN2YJftHSEmFO_zNIQ0LLXkaZ'

    function onSubmit(formId, action) {
      grecaptcha.execute(siteKey, { action }).then(
        function(token) {
          var formData = $('#' + formId).serializeArray().reduce(
            function(acc, x) { acc[x.name] = x.value.trim(); return acc; },
            {}
          )
          var data = Object.assign({}, formData, { token })
          $.post('form.php', data, function(response) {
            if (response.message !== undefined) {
              iziToast[response.type]({
                message: response.message,
                position: 'topCenter'
              });
            } else {
              iziToast.error({
                message: 'Coś poszło nie tak!',
                position: 'topCenter'
              });
            }
          })
        }
      );
    }

    $('#contact-form .submitButton').click(function() {
      onSubmit('contact-form', 'contact')
    })
	});
})(jQuery);
