document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: [ 'dayGrid', 'googleCalendar' ],
    header: {
      left: 'prev,next',
      center: 'title',
      right: ''
    },
    googleCalendarApiKey: 'AIzaSyCqVT_gguOrAj8QBxZwAaG5nH5MeoRk63M',
    events: 'l5u811nje3ijtl3g4j73l407e8@group.calendar.google.com',
    height: 550,
    width: 780,
    firstDay: 1,
    locale: 'pl',
    timeZone: 'Europe/Warsaw'
  });

  calendar.render();
});
