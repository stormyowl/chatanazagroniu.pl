<?php

require_once('Mail.php');

$config = parse_ini_file("config/settings.ini");

function verifyRecaptchaResponse($token, $secret, $action, $threshold) {
  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $recaptcha = json_decode(file_get_contents($url . '?secret=' . $secret . '&response=' . $token));
  return $recaptcha->{'action'} == $action && $recaptcha->{'score'} >= $threshold;
}



if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  header('Content-Type: application/json');

  if(!isset($_POST['token'])) {
    http_response_code(200);
    echo json_encode(array(
      "message" => "Rozwiązanie reCAPTCHY jest obowiązkowym parametrem.",
      "type" => "error",
    ));
    exit();
  }

  $verification = verifyRecaptchaResponse($_POST['token'], $config['secret'], 'contact', 0.5);
  if(!$verification) {
    http_response_code(200);
    echo json_encode(array(
      "message" => "Czy na pewno nie jesteś robotem? Jeśli nie to może wyślesz wiadomość e-mail?",
      "type" => "warning",
    ));
    exit();
  }

  $name = filter_input(INPUT_POST, 'name');
	$email = filter_input(INPUT_POST, 'email');
	$subject = filter_input(INPUT_POST, 'subject');
	$message = filter_input(INPUT_POST, 'message');

  $email_regexp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  $validation_errors = array();

  if(!(strlen($name) > 0)) {
		$validation_errors['name'] = 'Imię jest wymaganym polem.';
	}

  if(!(strlen($email) > 0)) {
		$validation_errors['email'] = 'E-mail jest wymaganym polem.';
	} elseif(!preg_match($email_regexp, $email)) {
    $validation_errors['email'] = 'Podany adres email jest niepoprawny.';
  }

  if(!(strlen($subject) > 0)) {
		$validation_errors['subject'] = 'Temat wiadomości jest wymaganym polem.';
	}

  if(!(strlen($message) > 0)) {
		$validation_errors['message'] = 'Wiadomość jest wymaganym polem.';
	}

	if(sizeof($validation_errors) > 0) {
    http_response_code(200);
    echo json_encode(array(
      "errors" => $validation_errors,
      "message" => "Formularz zawiera błędy walidacji.",
      "type" => "error"
    ));
    exit();
	}

	$body = $name.' napisał(a) wiadomość poprzez formularz na stronie Zagronia. Odpowiedz na tego maila aby skontaktować się z nadawcą.<br /><br />'.$message;

	$headers = array(
		'From' => $config['from'],
		'To' => $config['to'],
		'Subject' => $subject,
		'Reply-To' => '"'.$name.'" <'.$email.'>',
		'MIME-Version' => 1,
		'Content-type' => 'text/html;UTF-8'
	);

	$params = array(
		'host' => $config['host'],
		'port' => $config['port'],
		'auth' => true,
		'username' => $config['username'],
		'password' => $config['password']
	);

	$smtp = Mail::factory('smtp', $params);
	$mail = $smtp->send($config['to'], $headers, $body);

	if (PEAR::isError($mail)) {
    http_response_code(200);
    echo json_encode(array(
      "message" => "Nie udało się wysłać wiadomości! Może wyślesz wiadomość e-mail?",
      "type" => "error",
    ));
 	} else {
    http_response_code(200);
    echo json_encode(array(
      "message" => "Wiadomość została poprawnie wysłana.",
      "type" => "success",
    ));
 	}
} else {
	http_response_code(405);
  exit();
}

?>
